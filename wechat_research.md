# 以短史记为例获取公众号文章链接

## 准备

- 安装 **微信 Windows** 客户端
- 安装 **Fiddler** 用来截获微信客户端发送的 request



## 获取公众号历史消息列表

- Open Wechat and Fiddler
- Fiddler 有个功能可以只抓取Wechat窗口的包
- 打开目标公众号，这里以 短史记 为例
- 打开这个公众号的历史文章页面
- 在 Fiddler 中可以看到有 HTTP(S) 请求
    - HOST https://mp.weixin.qq.com
    - GET /mp/profile_ext?action=home
- 后面有几个完整的历史消息页面链接（均已过期）
- 在浏览器中打开这个链接。需要关闭Fiddler
- 滚动页面，载入更多历史文章
- 在浏览器中载入更多文章的操作不要太快，目测一分钟不要超过十次，否则会被禁止载入历史文章至少16个小时
- 在网页中载入所有需要的历史文章以后，保存页面HTML文件
- 把HTML文件放到目标目录中，程序会把页面中所有的文章链接下载到目标目录中
- 一篇公众号文章是一个目录，目录中有三个版本：原始网页，清洗格式版本，图片改成本地图片的版本



这个就是 短史记 公众号历史文章列表网页链接。这个链接会过期，有效期大概是4个小时。过期后重复以上步骤，可以得到新的链接。链接中的细节在后面分析。


```
https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzIzNTAyODMwOA==&scene=124&uin=NDE0MjI1Mzk1&key=1778891bcc0bf0d075c0542433bee3385d978b3313e5fa2a1b67883fa4d24a6d557a49c49a6491c0da3464525910622c46600dd024d920a675ba663efc932d9a47b882e7a42eb68ee473837e74edc958&devicetype=Windows+8&version=62050043&lang=en&a8scene=7&pass_ticket=5ar%2BSyR7DvnCAClfnabBsPBQWuE6FiYpMJhu0eZhH0JNCjgNtTC5dd4UgWa7yART&winzoom=0.875

https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzIzNTAyODMwOA==&scene=124&uin=NDE0MjI1Mzk1&key=05549438cefd734abd789ef09012994693a2911588178681c271895413786edab749ba84cdcf0189d386fd1f0d8156f4562fd7e276e6f530f68ae7a83877a62fd9e4f9475e12aa004264a3b40f9f1029&devicetype=Windows+8&version=62050043&lang=en&a8scene=7&pass_ticket=5ar%2BSyR7DvnCAClfnabBsPBQWuE6FiYpMJhu0eZhH0JNCjgNtTC5dd4UgWa7yART&winzoom=0.875

https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzIzNTAyODMwOA==&scene=124&uin=NDE0MjI1Mzk1&key=05549438cefd734a10aecce5e5cae67750fb54aa9cdd4613a512c10887260cec89950224d053468d7d30f68925d7fc934a6939ee043aef1a13e4acc735ab386093741f377be8a631a6237eb954b67a4d&devicetype=Windows+8&version=62050043&lang=en&a8scene=7&pass_ticket=%2BQWF9F0XqwdODL8pwmSVYVzeF6eWvT19FT2y7fipSUF68f3vZ7uRp%2B%2FQaCefRIo%2B&winzoom=0.875


https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzIzNTAyODMwOA==&scene=124&uin=NDE0MjI1Mzk1&key=05549438cefd734af078cbad81c0813201396411f884b897c4f48eb31b3e41210be35d618f2b070b8008ea510f962eacbfe4055533f54d6d7e29a7ba777aa10f12b45c71046a681f05ce91001ae83858&devicetype=Windows+8&version=62050043&lang=en&a8scene=7&pass_ticket=%2BQWF9F0XqwdODL8pwmSVYVzeF6eWvT19FT2y7fipSUF68f3vZ7uRp%2B%2FQaCefRIo%2B&winzoom=0.875
```

打开link

XPath of history message list container
//*[@id="js_history_list"]

XPath of the first message in the list
//*[@id="js_history_list"]/div[1]


get more msg request
```
https://mp.weixin.qq.com/mp/profile_ext?action=getmsg&__biz=MzIzNTAyODMwOA==&f=json&offset=10&count=10&is_ok=1&scene=124&uin=NDE0MjI1Mzk1&key=05549438cefd734af078cbad81c0813201396411f884b897c4f48eb31b3e41210be35d618f2b070b8008ea510f962eacbfe4055533f54d6d7e29a7ba777aa10f12b45c71046a681f05ce91001ae83858&pass_ticket=%2BQWF9F0XqwdODL8pwmSVYVzeF6eWvT19FT2y7fipSUF68f3vZ7uRp%2B%2FQaCefRIo%2B&wxtoken=&appmsg_token=921_X5PkdUqYYMTC7rJDK4t-4U0FnYP8yDYFTvRqLg~~&x5=0&f=json
```


## Tips
历史文章列表只显示最近的几篇文章，载入更多操作每次只能载入十篇，而且载入操作很频繁的话，会有操作频繁的错误提示，之后就不能打开历史消息页面了



## Appendix

### The source code of official account history message page

 ```HTML
 <html><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">


        <script nonce="" type="text/javascript">
            window.logs = {
                pagetime: {}
            };
            window.logs.pagetime['html_begin'] = (+new Date());
        </script>
        
        <link rel="dns-prefetch" href="//res.wx.qq.com">
<link rel="dns-prefetch" href="//mmbiz.qpic.cn">
<link rel="shortcut icon" type="image/x-icon" href="//res.wx.qq.com/mmbizwap/en_US/htmledition/images/icon/common/favicon22c41c.ico">
<script nonce="" type="text/javascript">
    String.prototype.html = function(encode) {
        var replace =["&#39;", "'", "&quot;", '"', "&nbsp;", " ", "&gt;", ">", "&lt;", "<", "&amp;", "&", "&yen;", "¥"];
        if (encode) {
            replace.reverse();
        }
        for (var i=0,str=this;i< replace.length;i+= 2) {
             str=str.replace(new RegExp(replace[i],'g'),replace[i+1]);
        }
        return str;
    };

    window.isInWeixinApp = function() {
        return /MicroMessenger/.test(navigator.userAgent);
    };

    window.getQueryFromURL = function(url) {
        url = url || 'http://qq.com/s?a=b#rd'; 
        var tmp = url.split('?'),
            query = (tmp[1] || "").split('#')[0].split('&'),
            params = {};
        for (var i=0; i<query.length; i++) {
            var arg = query[i].split('=');
            params[arg[0]] = arg[1];
        }
        if (params['pass_ticket']) {
        	params['pass_ticket'] = encodeURIComponent(params['pass_ticket'].html(false).html(false).replace(/\s/g,"+"));
        }
        return params;
    };

    (function() {
	    var params = getQueryFromURL(location.href);
        window.uin = params['uin'] || "NDE0MjI1Mzk1" || '';
        window.key = params['key'] || "05549438cefd734a10aecce5e5cae67750fb54aa9cdd4613a512c10887260cec89950224d053468d7d30f68925d7fc934a6939ee043aef1a13e4acc735ab386093741f377be8a631a6237eb954b67a4d" || '';
        window.wxtoken = params['wxtoken'] || '';
        window.pass_ticket = params['pass_ticket'] || '';
        window.appmsg_token = "921_Wngdd9ml4tfSmX/PIeTe-lVPkCev59jp2jy9wA~~";
    })();

    function wx_loaderror() {
        if (location.pathname === '/bizmall/reward') {
            new Image().src = '/mp/jsreport?key=96&content=reward_res_load_err&r=' + Math.random();
        }
    }

</script>

        <title></title>
        
<link onerror="wx_loaderror(this)" rel="stylesheet" href="https://res.wx.qq.com/open/libs/weui/0.2.0/weui.css">
<link onerror="wx_loaderror(this)" rel="stylesheet" href="https://res.wx.qq.com/open/libs/weui/1.1.1/weui.css">

<link rel="stylesheet" type="text/css" href="//res.wx.qq.com/mmbizwap/en_US/htmledition/style/page/profile/index37dc47.css">

    <script src="https://res.wx.qq.com/mmbizwap/en_US/htmledition/js/common/zepto1b4b91.js" type="text/javascript" async=""></script><style id="style-1-cropbar-clipper">/* Copyright 2014 Evernote Corporation. All rights reserved. */
.en-markup-crop-options {
    top: 18px !important;
    left: 50% !important;
    margin-left: -100px !important;
    width: 200px !important;
    border: 2px rgba(255,255,255,.38) solid !important;
    border-radius: 4px !important;
}

.en-markup-crop-options div div:first-of-type {
    margin-left: 0px !important;
}
</style></head>
    <body id="" class="en_US ">
        
<script type="text/javascript">
    if (window.location != window.parent.location) { 
        window.location.href = 'http://mp.weixin.qq.com/mp/readtemplate?t=wxm-cannot-open#wechat_redirect';
    }
    var pass_ticket = "+QWF9F0XqwdODL8pwmSVYVzeF6eWvT19FT2y7fipSUF68f3vZ7uRp+/QaCefRIo+" || ""; 
    var uin = "NDE0MjI1Mzk1" || ""; 
    var key = "05549438cefd734a10aecce5e5cae67750fb54aa9cdd4613a512c10887260cec89950224d053468d7d30f68925d7fc934a6939ee043aef1a13e4acc735ab386093741f377be8a631a6237eb954b67a4d" || ""; 
</script>

<div class="weui-search-bar" title="搜索" role="search" aria-label="搜索公众号文章" id="js_search" style="display:none;">
    <div class="weui-search-bar__form" style="-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;">
        <label class="weui-search-bar__label">
            <i class="weui-icon-search"></i>
            <span>搜索</span>
        </label>
    </div>
</div>
<div class="profile_info appmsg">
    <span class="radius_avatar profile_avatar">
        <img src="http://wx.qlogo.cn/mmhead/Q3auHgzwzM6NBSicQ1Lx4G9mN5yPZBicCFcaWJhEOtZpTHR1lLtVH8yg/0" id="icon">
    </span>
    <strong class="profile_nickname" id="nickname">
        短史记    </strong>
        <p class="profile_desc">
                短史记栏目。            </p>
    <div class="profile_opr" style="display: none;" id="js_button">
        <a href="javascript:void(0);" id="js_btn_add_contact" class="weui_btn weui_btn_plain_primary" style="display:none;">关注</a>
        <a href="javascript:void(0);" id="js_btn_view_profile" class="weui_btn weui_btn_plain_primary">发消息</a>
    </div>
    
</div>

<script type="text/javascript">
    var is_ok = 1; 
    var scene = "124" || ""; 
    var a8scene = "7"; 

    (function() {
        var is_android = /(Android)/i.test(navigator.userAgent);
        
        var __JSAPI__ = {
            ready: function(onBridgeReady) {
                var _onBridgeReady = function() {
                    if (!!onBridgeReady) {
                        onBridgeReady();
                    }
                };
                
                if (typeof top.window.WeixinJSBridge == "undefined" || !top.window.WeixinJSBridge.invoke) {
                    
                    if (top.window.document.addEventListener) {
                        top.window.document.addEventListener('WeixinJSBridgeReady', _onBridgeReady, false);
                    } else if (top.window.document.attachEvent) {
                        top.window.document.attachEvent('WeixinJSBridgeReady', _onBridgeReady); 
                        top.window.document.attachEvent('onWeixinJSBridgeReady', _onBridgeReady);
                    }
                } else {
                    
                    _onBridgeReady();
                }
            },
            invoke: function(methodName, args, callback) {
                this.ready(function() {
                    
                    if (typeof top.window.WeixinJSBridge != "object" ) {
                        alert("请在微信中打开此链接！");
                        return false;
                    }
                    top.window.WeixinJSBridge.invoke(methodName, args, function(ret){
                        if (!!callback) {
                            callback.apply(window, arguments);
                            var err_msg = ret && ret.err_msg ? ", err_msg-> " + ret.err_msg : "";
                            console.info("[jsapi] invoke->" + methodName + err_msg);
                        }
                    });
                });
            }
        }
        var __Ajax__ = function(obj) {

            var type = (obj.type || 'GET').toUpperCase();
            var async = typeof obj.async == 'undefined' ? true : obj.async;
            var url = obj.url;
            var xhr = new XMLHttpRequest();
            var timer = null;
            var data = null;

            if (typeof obj.data == "object"){
                var d = obj.data;
                data = [];
                for(var k in d) {
                    if (d.hasOwnProperty(k)){
                        data.push(k + "=" + encodeURIComponent(d[k]));
                    }
                }
                data = data.join("&");
            }else{
                data = typeof obj.data  == 'string' ? obj.data : null;
            }
            
            xhr.open(type, url, async);
            var _onreadystatechange = xhr.onreadystatechange; 

            xhr.onreadystatechange = function() {
                
                if (typeof _onreadystatechange == 'function') {
                    _onreadystatechange.apply(xhr);
                }
                if ( xhr.readyState == 3 ) {
                    obj.received && obj.received(xhr);
                }
                if ( xhr.readyState == 4 ) {
                    xhr.onreadystatechange = null;
                    var status = xhr.status;
                    if ( status >= 200 && status < 400 ) {
                        var responseText = xhr.responseText;
                        var resp = responseText;
                        if (obj.dataType == 'json'){
                            try{
                                resp = eval("(" + resp + ")");
                            }catch(e){
                                obj.error && obj.error(xhr);
                                return;
                            }
                        }
                        obj.success && obj.success(resp);
                    } else {
                        obj.error && obj.error(xhr);
                    }
                    clearTimeout(timer);
                    obj.complete && obj.complete();
                    obj.complete = null;
                }
            };
            if( type == 'POST' ){
                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            }
            xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            if( typeof obj.timeout != 'undefined' ){
                timer = setTimeout(function(){
                    xhr.abort("timeout");
                    obj.complete && obj.complete();
                    obj.complete = null;
                }, obj.timeout);
            }
            try{
                xhr.send(data);
            } catch ( e ) {
                obj.error && obj.error();
            }
        }
        if (navigator.userAgent.indexOf("WindowsWechat") != -1) { 
            __Ajax__({
                type: 'POST',
                dataType: 'json',
                url: '/mp/profile_ext?action=urlcheck&uin=' + window.uin + '&key=' + window.key + '&pass_ticket=' + window.pass_ticket,
                data: {
                    __biz: 'MzIzNTAyODMwOA==',
                    scene: scene,
                    url_list: ''
                },
                success: function(res) {}
            });
            return ;
        }
        __JSAPI__.invoke('getRouteUrl', {}, function(res) { 
            if (res.err_msg.indexOf('ok') != -1) {
                var url = res.urls;
                if (is_android) {
                    url = JSON.parse(url);
                }
                __Ajax__({
                    type: 'POST',
                    dataType: 'json',
                    url: '/mp/profile_ext?action=urlcheck&uin=' + window.uin + '&key=' + window.key + '&pass_ticket=' + window.pass_ticket,
                    data: {
                        __biz: 'MzIzNTAyODMwOA==',
                        scene: scene,
                        url_list: JSON.stringify({url_list: url})
                    },
                    success: function(res) {
                        if (res.is_ok == 0) {
                            document.getElementById('js_button').style.display = 'none';
                            is_ok = 0;
                        } else {
                            document.getElementById('js_button').style.display = '';
                            is_ok = 1;
                        }
                    }
                });
            } else { 
                document.getElementById('js_button').style.display = '';
                __Ajax__({
                    type: 'POST',
                    dataType: 'json',
                    url: '/mp/profile_ext?action=urlcheck&uin=' + window.uin + '&key=' + window.key + '&pass_ticket=' + window.pass_ticket,
                    data: {
                        __biz: 'MzIzNTAyODMwOA==',
                        scene: scene,
                        url_list: ''
                    },
                    success: function(res) {}
                });
            }
        });
    })();
</script>

<div class="weui_category_title js_tag">所有消息</div>
<div id="js_container"><div id="js_profile_history_container">

    <div class="weui_msg_card_list" id="js_history_list">
    
        
        <div class="weui_msg_card js_card" msgid="1000000436">
            <div class="weui_msg_card_hd">2017年9月5日</div>
            <div class="weui_msg_card_bd">
                 
                    
                         
                        <div id="WXAPPMSG1000000436" class="weui_media_box appmsg js_appmsg" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207833&amp;idx=1&amp;sn=688a1dad339789efa88c328dab56c91a&amp;chksm=f0ef3c62c798b574089319309b6b0c78fc8142edabe15d727f7f8b523cd0ca0c2b877fb47bb5&amp;scene=38#wechat_redirect">
                            
                            <span class="weui_media_hd js_media" style="background-image:url(http://mmbiz.qpic.cn/mmbiz_jpg/ibYIE3gDV7K5J7Ll5H2IYNFgn94W20QNJZVrKEleac6NHUicrPibD5Z0pvUA2iaaBUWdUxrAvynppKrZ51ZQicoJeHw/0?wx_fmt=jpeg)" data-s="640" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207833&amp;idx=1&amp;sn=688a1dad339789efa88c328dab56c91a&amp;chksm=f0ef3c62c798b574089319309b6b0c78fc8142edabe15d727f7f8b523cd0ca0c2b877fb47bb5&amp;scene=38#wechat_redirect" data-type="APPMSG">
                              
                              
                            </span>
                            
                            <div class="weui_media_bd js_media" data-type="APPMSG">
                                <h4 class="weui_media_title" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207833&amp;idx=1&amp;sn=688a1dad339789efa88c328dab56c91a&amp;chksm=f0ef3c62c798b574089319309b6b0c78fc8142edabe15d727f7f8b523cd0ca0c2b877fb47bb5&amp;scene=38#wechat_redirect">
                                <span id="copyright_logo" class="icon_original_tag">原创</span>
                                大陆第一份“流行音乐排行榜”风波 | 短史记
                                </h4>
                                <p class="weui_media_desc">回望80年代。</p>
                                <p class="weui_media_extra_info">2017年9月5日<span id="copyright_logo" class="icon_original_tag">原创</span></p>
                            </div>
                        </div>
                        
                    
                

                
            </div>
        </div>
        
    

    
        
        <div class="weui_msg_card js_card" msgid="1000000435">
            <div class="weui_msg_card_hd">2017年9月4日</div>
            <div class="weui_msg_card_bd">
                 
                    
                         
                        <div id="WXAPPMSG1000000435" class="weui_media_box appmsg js_appmsg" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207828&amp;idx=1&amp;sn=848b6e30f39cc95be46b2423672b4fd3&amp;chksm=f0ef3c6fc798b579d4c541ac9d6906483eff3e67241effb92bad7cda4b8cb0086344d2e9eff5&amp;scene=38#wechat_redirect">
                            
                            <span class="weui_media_hd js_media" style="background-image:url(http://mmbiz.qpic.cn/mmbiz_jpg/ibYIE3gDV7K7ibvVQcSqkXPNvCd8GiarvQExBXN7LDozmKGLQXuAp9FIhFgav5fDEGCVEn1XS5APpIAo9XfdUhMfA/0?wx_fmt=jpeg)" data-s="640" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207828&amp;idx=1&amp;sn=848b6e30f39cc95be46b2423672b4fd3&amp;chksm=f0ef3c6fc798b579d4c541ac9d6906483eff3e67241effb92bad7cda4b8cb0086344d2e9eff5&amp;scene=38#wechat_redirect" data-type="APPMSG">
                              
                              
                            </span>
                            
                            <div class="weui_media_bd js_media" data-type="APPMSG">
                                <h4 class="weui_media_title" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207828&amp;idx=1&amp;sn=848b6e30f39cc95be46b2423672b4fd3&amp;chksm=f0ef3c6fc798b579d4c541ac9d6906483eff3e67241effb92bad7cda4b8cb0086344d2e9eff5&amp;scene=38#wechat_redirect">
                                <span id="copyright_logo" class="icon_original_tag">原创</span>
                                新版历史教科书：内中插图大有问题 | 短史记
                                </h4>
                                <p class="weui_media_desc">文字要求真，插图也同样要求真。</p>
                                <p class="weui_media_extra_info">2017年9月4日<span id="copyright_logo" class="icon_original_tag">原创</span></p>
                            </div>
                        </div>
                        
                    
                

                
            </div>
        </div>
        
    

    
        
        <div class="weui_msg_card js_card" msgid="1000000434">
            <div class="weui_msg_card_hd">2017年9月3日</div>
            <div class="weui_msg_card_bd">
                 
                    
                         
                        <div id="WXAPPMSG1000000434" class="weui_media_box appmsg js_appmsg" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207825&amp;idx=1&amp;sn=63da3ca8e3faba8ad9296982b8ef5d16&amp;chksm=f0ef3c6ac798b57c9f4bb5f3e0734a5e91b62c38bb05ffe2d2e44a16e168dc0d14917336d58c&amp;scene=38#wechat_redirect">
                            
                            <span class="weui_media_hd js_media" style="background-image:url(http://mmbiz.qpic.cn/mmbiz_jpg/ibYIE3gDV7K7ibvVQcSqkXPNvCd8GiarvQEpW37TibQfHA4EISwtweUQ0oxHs7pCeldwV2NDCJyVb1J5Q8uVarzTKA/0?wx_fmt=jpeg)" data-s="640" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207825&amp;idx=1&amp;sn=63da3ca8e3faba8ad9296982b8ef5d16&amp;chksm=f0ef3c6ac798b57c9f4bb5f3e0734a5e91b62c38bb05ffe2d2e44a16e168dc0d14917336d58c&amp;scene=38#wechat_redirect" data-type="APPMSG">
                              
                              
                            </span>
                            
                            <div class="weui_media_bd js_media" data-type="APPMSG">
                                <h4 class="weui_media_title" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207825&amp;idx=1&amp;sn=63da3ca8e3faba8ad9296982b8ef5d16&amp;chksm=f0ef3c6ac798b57c9f4bb5f3e0734a5e91b62c38bb05ffe2d2e44a16e168dc0d14917336d58c&amp;scene=38#wechat_redirect">
                                <span id="copyright_logo" class="icon_original_tag">原创</span>
                                百年前的原则：警察不可用大炮打麻雀 | 短史记
                                </h4>
                                <p class="weui_media_desc">违停。</p>
                                <p class="weui_media_extra_info">2017年9月3日<span id="copyright_logo" class="icon_original_tag">原创</span></p>
                            </div>
                        </div>
                        
                    
                

                
            </div>
        </div>
        
    

    
        
        <div class="weui_msg_card js_card" msgid="1000000433">
            <div class="weui_msg_card_hd">2017年9月2日</div>
            <div class="weui_msg_card_bd">
                 
                    
                         
                        <div id="WXAPPMSG1000000433" class="weui_media_box appmsg js_appmsg" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207820&amp;idx=1&amp;sn=c5c92ff927ed7d961116cfd2c10b068d&amp;chksm=f0ef3c77c798b5617bddb2dacd19efcc6725273e4064588c0efd05c8b22db9dd8d2eb01566f1&amp;scene=38#wechat_redirect">
                            
                            <span class="weui_media_hd js_media" style="background-image:url(http://mmbiz.qpic.cn/mmbiz_jpg/ibYIE3gDV7K7Kq2tmLh2wiblfa88ldicZzZ0HoP8skH0icRB8nt6maXqScJBhGcqoX0edFp61XqaqtEVDuk4braPicA/0?wx_fmt=jpeg)" data-s="640" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207820&amp;idx=1&amp;sn=c5c92ff927ed7d961116cfd2c10b068d&amp;chksm=f0ef3c77c798b5617bddb2dacd19efcc6725273e4064588c0efd05c8b22db9dd8d2eb01566f1&amp;scene=38#wechat_redirect" data-type="APPMSG">
                              
                              
                            </span>
                            
                            <div class="weui_media_bd js_media" data-type="APPMSG">
                                <h4 class="weui_media_title" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207820&amp;idx=1&amp;sn=c5c92ff927ed7d961116cfd2c10b068d&amp;chksm=f0ef3c77c798b5617bddb2dacd19efcc6725273e4064588c0efd05c8b22db9dd8d2eb01566f1&amp;scene=38#wechat_redirect">
                                
                                抗战胜利的核心遗产：拥抱国际主流社会 | 短史记
                                </h4>
                                <p class="weui_media_desc">今天是抗战胜利纪念日。</p>
                                <p class="weui_media_extra_info">2017年9月2日</p>
                            </div>
                        </div>
                        
                    
                

                
            </div>
        </div>
        
    

    
        
        <div class="weui_msg_card js_card" msgid="1000000432">
            <div class="weui_msg_card_hd">2017年9月1日</div>
            <div class="weui_msg_card_bd">
                 
                    
                         
                        <div id="WXAPPMSG1000000432" class="weui_media_box appmsg js_appmsg" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207813&amp;idx=1&amp;sn=5652beae29100ecea8b00af6b2f3be31&amp;chksm=f0ef3c7ec798b568a9d5425a6b79bf21ac33ff04ff4081850844502566937bc4753c08cf92ec&amp;scene=38#wechat_redirect">
                            
                            <span class="weui_media_hd js_media" style="background-image:url(http://mmbiz.qpic.cn/mmbiz_jpg/ibYIE3gDV7K57rZSXe6KhGgAkpeaRzQ9V44iabzNIDpBh4DqYmYPzyobaFZ4BKTAObpCdIcNDXxAgZXEUlT580sA/0?wx_fmt=jpeg)" data-s="640" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207813&amp;idx=1&amp;sn=5652beae29100ecea8b00af6b2f3be31&amp;chksm=f0ef3c7ec798b568a9d5425a6b79bf21ac33ff04ff4081850844502566937bc4753c08cf92ec&amp;scene=38#wechat_redirect" data-type="APPMSG">
                              
                              
                            </span>
                            
                            <div class="weui_media_bd js_media" data-type="APPMSG">
                                <h4 class="weui_media_title" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207813&amp;idx=1&amp;sn=5652beae29100ecea8b00af6b2f3be31&amp;chksm=f0ef3c7ec798b568a9d5425a6b79bf21ac33ff04ff4081850844502566937bc4753c08cf92ec&amp;scene=38#wechat_redirect">
                                <span id="copyright_logo" class="icon_original_tag">原创</span>
                                《国歌法》即将施行，回顾国歌六十年命运浮沉 | 短史记
                                </h4>
                                <p class="weui_media_desc">国歌歌词多次变更。</p>
                                <p class="weui_media_extra_info">2017年9月1日<span id="copyright_logo" class="icon_original_tag">原创</span></p>
                            </div>
                        </div>
                        
                    
                

                
            </div>
        </div>
        
    

    
        
        <div class="weui_msg_card js_card" msgid="1000000431">
            <div class="weui_msg_card_hd">2017年8月31日</div>
            <div class="weui_msg_card_bd">
                 
                    
                         
                        <div id="WXAPPMSG1000000431" class="weui_media_box appmsg js_appmsg" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207809&amp;idx=1&amp;sn=fc09816077922674c498fc1ddb9da2da&amp;chksm=f0ef3c7ac798b56c6344b1c09cfc5f79ced78b9d17eefe104ab1b15d79ce8372df1f9a3b890d&amp;scene=38#wechat_redirect">
                            
                            <span class="weui_media_hd js_media" style="background-image:url(http://mmbiz.qpic.cn/mmbiz_jpg/ibYIE3gDV7K4UrBl3dbkIZJGh81GbLwUhrJ47lXPH6VzFANDA7Ne2eXsB8aQgszz9QnRg0ia9fQLrh8gMicyib3Irw/0?wx_fmt=jpeg)" data-s="640" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207809&amp;idx=1&amp;sn=fc09816077922674c498fc1ddb9da2da&amp;chksm=f0ef3c7ac798b56c6344b1c09cfc5f79ced78b9d17eefe104ab1b15d79ce8372df1f9a3b890d&amp;scene=38#wechat_redirect" data-type="APPMSG">
                              
                              
                            </span>
                            
                            <div class="weui_media_bd js_media" data-type="APPMSG">
                                <h4 class="weui_media_title" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207809&amp;idx=1&amp;sn=fc09816077922674c498fc1ddb9da2da&amp;chksm=f0ef3c7ac798b56c6344b1c09cfc5f79ced78b9d17eefe104ab1b15d79ce8372df1f9a3b890d&amp;scene=38#wechat_redirect">
                                <span id="copyright_logo" class="icon_original_tag">原创</span>
                                语文教材，究竟删减了多少鲁迅作品？
                                </h4>
                                <p class="weui_media_desc">鲁迅文章在教科书中被删减，原因何在？</p>
                                <p class="weui_media_extra_info">2017年8月31日<span id="copyright_logo" class="icon_original_tag">原创</span></p>
                            </div>
                        </div>
                        
                    
                

                
            </div>
        </div>
        
    

    
        
        <div class="weui_msg_card js_card" msgid="1000000430">
            <div class="weui_msg_card_hd">2017年8月30日</div>
            <div class="weui_msg_card_bd">
                 
                    
                         
                        <div id="WXAPPMSG1000000430" class="weui_media_box appmsg js_appmsg" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207802&amp;idx=1&amp;sn=ca1732420201717b86ae8f2524d1ebb9&amp;chksm=f0ef3c01c798b5178e31771120fc0eb54059acfa0a612e9e4a519b80906d91938263bc7ca810&amp;scene=38#wechat_redirect">
                            
                            <span class="weui_media_hd js_media" style="background-image:url(http://mmbiz.qpic.cn/mmbiz_jpg/ibYIE3gDV7K6NYHkE6wOzmLH2WrcGa9V5FyjcDELhBgvwKjyiccBsQs1gecKxMCNAkhk7tlysQyWPkBU8Yez29MQ/0?wx_fmt=jpeg)" data-s="640" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207802&amp;idx=1&amp;sn=ca1732420201717b86ae8f2524d1ebb9&amp;chksm=f0ef3c01c798b5178e31771120fc0eb54059acfa0a612e9e4a519b80906d91938263bc7ca810&amp;scene=38#wechat_redirect" data-type="APPMSG">
                              
                              
                            </span>
                            
                            <div class="weui_media_bd js_media" data-type="APPMSG">
                                <h4 class="weui_media_title" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207802&amp;idx=1&amp;sn=ca1732420201717b86ae8f2524d1ebb9&amp;chksm=f0ef3c01c798b5178e31771120fc0eb54059acfa0a612e9e4a519b80906d91938263bc7ca810&amp;scene=38#wechat_redirect">
                                <span id="copyright_logo" class="icon_original_tag">原创</span>
                                “彭德怀打金日成耳光”之说很可疑 | 短史记
                                </h4>
                                <p class="weui_media_desc">目前流传的版本，经不起史料检验。</p>
                                <p class="weui_media_extra_info">2017年8月30日<span id="copyright_logo" class="icon_original_tag">原创</span></p>
                            </div>
                        </div>
                        
                    
                

                
            </div>
        </div>
        
    

    
        
        <div class="weui_msg_card js_card" msgid="1000000429">
            <div class="weui_msg_card_hd">2017年8月29日</div>
            <div class="weui_msg_card_bd">
                 
                    
                         
                        <div id="WXAPPMSG1000000429" class="weui_media_box appmsg js_appmsg" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207798&amp;idx=1&amp;sn=67bb56003db45562461b355477a553e2&amp;chksm=f0ef3c0dc798b51bf9c67cd9a3245b6297f0a3a08a930f831a61f62ada6145b338cdd61c4738&amp;scene=38#wechat_redirect">
                            
                            <span class="weui_media_hd js_media" style="background-image:url(http://mmbiz.qpic.cn/mmbiz_jpg/ibYIE3gDV7K7gpovO7kCp578I7BIjWORK287n655qkI32fM9DxyZtEYPK1IibC2DibxicNVnQ2PxVEW5E23WoH4fKg/0?wx_fmt=jpeg)" data-s="640" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207798&amp;idx=1&amp;sn=67bb56003db45562461b355477a553e2&amp;chksm=f0ef3c0dc798b51bf9c67cd9a3245b6297f0a3a08a930f831a61f62ada6145b338cdd61c4738&amp;scene=38#wechat_redirect" data-type="APPMSG">
                              
                              
                            </span>
                            
                            <div class="weui_media_bd js_media" data-type="APPMSG">
                                <h4 class="weui_media_title" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207798&amp;idx=1&amp;sn=67bb56003db45562461b355477a553e2&amp;chksm=f0ef3c0dc798b51bf9c67cd9a3245b6297f0a3a08a930f831a61f62ada6145b338cdd61c4738&amp;scene=38#wechat_redirect">
                                <span id="copyright_logo" class="icon_original_tag">原创</span>
                                人工智能进入小学课程，曾长期被打成“伪科学” | 短史记
                                </h4>
                                <p class="weui_media_desc">这一代小学生，左手“人工智能”，右手“黄帝内经，你们怕不怕？</p>
                                <p class="weui_media_extra_info">2017年8月29日<span id="copyright_logo" class="icon_original_tag">原创</span></p>
                            </div>
                        </div>
                        
                    
                

                
            </div>
        </div>
        
    

    
        
        <div class="weui_msg_card js_card" msgid="1000000428">
            <div class="weui_msg_card_hd">2017年8月28日</div>
            <div class="weui_msg_card_bd">
                 
                    
                         
                        <div id="WXAPPMSG1000000428" class="weui_media_box appmsg js_appmsg" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207792&amp;idx=1&amp;sn=268d83b1ad0cb2a05593759315e8ea87&amp;chksm=f0ef3c0bc798b51d54f0703a82e8ac7f83555e1231cdd4a6710e6dfb55f3efd93b4e61ba9f7b&amp;scene=38#wechat_redirect">
                            
                            <span class="weui_media_hd js_media" style="background-image:url(http://mmbiz.qpic.cn/mmbiz_jpg/ibYIE3gDV7K5lpbg7O3RRibj70ibguBRW3ic4aJ94W8UMQtD8Wky5sduIrh2UOibLeYEiajBFQVAk9pvnVzvFBWsRu4Q/0?wx_fmt=jpeg)" data-s="640" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207792&amp;idx=1&amp;sn=268d83b1ad0cb2a05593759315e8ea87&amp;chksm=f0ef3c0bc798b51d54f0703a82e8ac7f83555e1231cdd4a6710e6dfb55f3efd93b4e61ba9f7b&amp;scene=38#wechat_redirect" data-type="APPMSG">
                              
                              
                            </span>
                            
                            <div class="weui_media_bd js_media" data-type="APPMSG">
                                <h4 class="weui_media_title" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207792&amp;idx=1&amp;sn=268d83b1ad0cb2a05593759315e8ea87&amp;chksm=f0ef3c0bc798b51d54f0703a82e8ac7f83555e1231cdd4a6710e6dfb55f3efd93b4e61ba9f7b&amp;scene=38#wechat_redirect">
                                <span id="copyright_logo" class="icon_original_tag">原创</span>
                                “以法治孝”的秦王朝，反而不孝成风 | 短史记
                                </h4>
                                <p class="weui_media_desc">秦人“不孝”的根本原因，在于秦制，在于其弱民、贫民政策。</p>
                                <p class="weui_media_extra_info">2017年8月28日<span id="copyright_logo" class="icon_original_tag">原创</span></p>
                            </div>
                        </div>
                        
                    
                

                
            </div>
        </div>
        
    

    
        
        <div class="weui_msg_card js_card" msgid="1000000427">
            <div class="weui_msg_card_hd">2017年8月27日</div>
            <div class="weui_msg_card_bd">
                 
                    
                         
                        <div id="WXAPPMSG1000000427" class="weui_media_box appmsg js_appmsg" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207787&amp;idx=1&amp;sn=820bad445031f58ccc11eab7d745c6bb&amp;chksm=f0ef3c10c798b5063b54d2a8f16683b535381128c3870efd18952ee8e157b46301cba85e5106&amp;scene=38#wechat_redirect">
                            
                            <span class="weui_media_hd js_media" style="background-image:url(http://mmbiz.qpic.cn/mmbiz_jpg/ibYIE3gDV7K7h29TyW7MqdJQ8DEoF8lazarwKpySV9Q6qGKJLVxjciaJfZMOr9HYyiaF3gyDTdj5V18UQDZAqK6Dw/0?wx_fmt=jpeg)" data-s="640" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207787&amp;idx=1&amp;sn=820bad445031f58ccc11eab7d745c6bb&amp;chksm=f0ef3c10c798b5063b54d2a8f16683b535381128c3870efd18952ee8e157b46301cba85e5106&amp;scene=38#wechat_redirect" data-type="APPMSG">
                              
                              
                            </span>
                            
                            <div class="weui_media_bd js_media" data-type="APPMSG">
                                <h4 class="weui_media_title" hrefs="http://mp.weixin.qq.com/s?__biz=MzIzNTAyODMwOA==&amp;mid=2650207787&amp;idx=1&amp;sn=820bad445031f58ccc11eab7d745c6bb&amp;chksm=f0ef3c10c798b5063b54d2a8f16683b535381128c3870efd18952ee8e157b46301cba85e5106&amp;scene=38#wechat_redirect">
                                <span id="copyright_logo" class="icon_original_tag">原创</span>
                                蒋介石为何秘密给胡适送钱？ | 短史记
                                </h4>
                                <p class="weui_media_desc">此种内因，不可对外解释。</p>
                                <p class="weui_media_extra_info">2017年8月27日<span id="copyright_logo" class="icon_original_tag">原创</span></p>
                            </div>
                        </div>
                        
                    
                

                
            </div>
        </div>
        
    
</div>

    <div class="loadmore" id="js_loading">
        <div class="tips_wrp">
            <i class="icon_loading"></i>
            <span class="tips">正在加载</span>
        </div>
    </div>
    <div class="loadmore with_line" style="display: none;" id="js_nomore">
        <div class="tips_wrp">
            <span class="tips js_no_more_msg" style="display: none;">已无更多</span>
            <span class="tips js_need_add_contact" style="display: none;">关注公众帐号，接收更多消息</span>
        </div>
    </div>
</div>

<script type="text/html" id="js_profile_history_tpl">
{{each list as value keys}}
    {{if value && value.comm_msg_info}}
        {{if value.comm_msg_info.type==1}}    
        <div class="weui_msg_card js_card" msgid="{{value.comm_msg_info.id}}">
            <div class="weui_msg_card_hd">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日'}}</div>
            <div class="weui_msg_card_bd">
                <div class="weui_media_box text js_appmsg">
                    <div class="weui_media_bd js_media" data-type="TEXT">
                        
                        <div>
                            {{handleTextEmoji value.comm_msg_info.content}}
                        </div>
                    </div>
                    <div class="weui_media_ft">
                        <p class="weui_media_extra_info">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日'}}</p>
                    </div>
                </div>
            </div>
        </div>
        {{else if value.comm_msg_info.type==3}}    
        <div class="weui_msg_card js_card" msgid="{{value.comm_msg_info.id}}">
            <div class="weui_msg_card_hd">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日'}}</div>
            <div class="weui_msg_card_bd">
                <div id="WXIMG{{value.comm_msg_info.id}}" class="weui_media_box img js_appmsg">
                    <div class="weui_media_bd js_media" data-type="IMG">
                        {{if value.image_msg_ext_info.cdn_url}}
                        <img src="{{value.image_msg_ext_info.cdn_url}}" data-msgid="{{value.comm_msg_info.id}}" data-s="640">
                        {{else}}
                        <img src="https://mp.weixin.qq.com/mp/getmediadata?__biz={{biz}}&type=img&mode=normal&msgid={{value.comm_msg_info.id}}&uin={{uin}}&key={{key}}&openkey={{openkey}}#wechat_redirect" data-msgid="{{value.comm_msg_info.id}}" data-s="640">
                        {{/if}}
                    </div>
                    <div class="weui_media_ft">
                        <p class="weui_media_extra_info">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日'}}</p>
                    </div>
                </div>
            </div>
        </div>
        {{else if value.comm_msg_info.type==34}}    
        <div class="weui_msg_card js_card" msgid="{{value.comm_msg_info.id}}">
            <div class="weui_msg_card_hd">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日'}}</div>
            <div class="weui_msg_card_bd">
                <div id="WXVOICE{{value.comm_msg_info.id}}" class="weui_media_box audio_msg js_appmsg">
                    <div class="weui_media_bd js_media" data-type="AUDIO">
                        <div class="weui_audio">
                            <div class="audio_content vm_item vm_item_primary">
                                <strong class="audio_title">
                                    {{if !value.voice_msg_ext_info.title}}
                                        语音
                                    {{else}}
                                        {{value.voice_msg_ext_info.title}}
                                    {{/if}}
                                </strong>
                                <p class="audio_desc"></p>
                                <audio fileid="{{value.voice_msg_ext_info.fileid}}" preload data-time='{{value.voice_msg_ext_info.play_length}}' src="/mp/getmediadata?__biz={{biz}}&type=voice&msgid={{value.comm_msg_info.id}}&uin={{uin}}&key={{key}}&openkey={{openkey}}">
                                    not support</audio>
                            </div>
                            <span class="audio_switch vm_item vm_item_defualt">
                                <i class="icon_audio_default"></i>
                                <i class="icon_audio_playing"></i>
                            </span>
                        </div>
                    </div>
                    <div class="weui_media_ft">
                        <p class="weui_media_extra_info">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日'}}</p>
                    </div>
                </div>
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
            </div>
        </div>
        {{else if value.comm_msg_info.type==49}}
        <div class="weui_msg_card js_card" msgid="{{value.comm_msg_info.id}}">
            <div class="weui_msg_card_hd">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日'}}</div>
            <div class="weui_msg_card_bd">
                {{if value.app_msg_ext_info.subtype == 9}} 
                    {{if value.app_msg_ext_info.del_flag == 4}} 
                    <div id="WXAPPMSG{{value.comm_msg_info.id}}" class="weui_media_box appmsg js_appmsg media_del" hrefs="{{value.app_msg_ext_info.content_url}}">
                        <span class="weui_media_hd js_media" hrefs="{{value.app_msg_ext_info.content_url}}" data-type="APPMSG"></span>
                        <div class="weui_media_bd js_media" data-type="APPMSG">
                            <h4 class="weui_media_title" hrefs="{{value.app_msg_ext_info.content_url}}">内容违规已被删除</h4>
                            <p class="weui_media_desc"></p>
                            <p class="weui_media_extra_info">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日'}}</p>
                        </div>
                    </div>
                    {{else}}
                        {{if value.app_msg_ext_info.item_show_type == 5}} 
                        <div id="WXAPPMSG{{value.comm_msg_info.id}}" class="weui_media_box appmsg js_appmsg video_msg" hrefs="{{value.app_msg_ext_info.content_url}}">
                            {{if value.app_msg_ext_info.cover}}
                            <span class="weui_media_hd js_media" style="background-image:url({{value.app_msg_ext_info.cover}})" data-s="640" hrefs="{{value.app_msg_ext_info.content_url}}" data-type="APPMSG"></span>
                            {{/if}}
                            <div class="weui_media_bd js_media" data-type="APPMSG">
                                <h4 class="weui_media_title" hrefs="{{value.app_msg_ext_info.content_url}}">
                                {{if value.app_msg_ext_info.copyright_stat==11}}<span id="copyright_logo" class="icon_original_tag">原创</span>{{/if}}
                                {{value.app_msg_ext_info.title}}  
                                </h4>
                                <p class="weui_media_desc">{{=value.app_msg_ext_info.digest}}</p>
                                <p class="weui_media_extra_info">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日'}}{{if value.app_msg_ext_info.copyright_stat==11}}<span id="copyright_logo" class="icon_original_tag">原创</span>{{/if}}</p>
                            </div>
                        </div>
                        {{else if value.app_msg_ext_info.item_show_type == 7}} 
                        <div id="WXVOICE{{value.comm_msg_info.id}}" class="weui_media_box audio_msg js_appmsg">
                            <div class="weui_media_bd js_media" data-type="AUDIO">
                                <div class="weui_audio">
                                    <div class="audio_content vm_item vm_item_primary">
                                        <strong class="audio_title">{{value.app_msg_ext_info.title}}</strong>
                                        <p class="audio_desc">{{=value.app_msg_ext_info.digest}}</p>
                                        <audio fileid="{{value.voice_msg_ext_info.fileid}}" preload data-time='{{value.voice_msg_ext_info.play_length}}' src="/mp/getmediadata?__biz={{biz}}&type=voice&msgid={{value.comm_msg_info.id}}&uin={{uin}}&key={{key}}&openkey={{openkey}}">
                                            not support</audio>
                                    </div>
                                    <span class="audio_switch vm_item vm_item_defualt">
                                        <i class="icon_audio_default"></i>
                                        <i class="icon_audio_playing"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="weui_media_ft">
                                <p class="weui_media_extra_info">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日'}}</p>
                            </div>
                        </div>
                        {{else if value.app_msg_ext_info.item_show_type == 8}} 
                        <div id="WXIMG{{value.comm_msg_info.id}}" class="weui_media_box img" class="weui_media_box img js_appmsg">
                            <div class="weui_media_bd js_media" data-type="IMG">
                                {{if value.image_msg_ext_info.cdn_url}}
                                <img src="{{value.image_msg_ext_info.cdn_url}}" data-msgid="{{value.comm_msg_info.id}}" data-s="640">
                                {{else}}
                                <img src="https://mp.weixin.qq.com/mp/getmediadata?__biz={{biz}}&type=img&mode=normal&msgid={{value.comm_msg_info.id}}&uin={{uin}}&key={{key}}&openkey={{openkey}}#wechat_redirect" data-msgid="{{value.comm_msg_info.id}}" data-s="640">
                                {{/if}}
                            </div>
                            <div class="weui_media_ft">
                                <p class="weui_media_extra_info">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日'}}</p>
                            </div>
                        </div>
                        {{else if value.app_msg_ext_info.item_show_type == 10}} 
                        <div class="weui_media_box text js_appmsg">
                            <div class="weui_media_bd js_media" data-type="TEXT">
                              
                              <div>
                                {{handleTextEmoji value.comm_msg_info.content}}
                              </div>
                            </div>
                            <div class="weui_media_ft">
                                <p class="weui_media_extra_info">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日'}}</p>
                            </div>
                        </div>
                        {{else}} 
                        <div id="WXAPPMSG{{value.comm_msg_info.id}}" class="weui_media_box appmsg js_appmsg" hrefs="{{value.app_msg_ext_info.content_url}}">
                            {{if value.app_msg_ext_info.cover}}
                            <span class="weui_media_hd js_media" style="background-image:url({{value.app_msg_ext_info.cover}})" data-s="640" hrefs="{{value.app_msg_ext_info.content_url}}" data-type="APPMSG">
                              
                              
                            </span>
                            {{/if}}
                            <div class="weui_media_bd js_media" data-type="APPMSG">
                                <h4 class="weui_media_title" hrefs="{{value.app_msg_ext_info.content_url}}">
                                {{if value.app_msg_ext_info.copyright_stat==11}}<span id="copyright_logo" class="icon_original_tag">原创</span>{{/if}}
                                {{value.app_msg_ext_info.title}}
                                </h4>
                                <p class="weui_media_desc">{{=value.app_msg_ext_info.digest}}</p>
                                <p class="weui_media_extra_info">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日'}}{{if value.app_msg_ext_info.copyright_stat==11}}<span id="copyright_logo" class="icon_original_tag">原创</span>{{/if}}</p>
                            </div>
                        </div>
                        {{/if}}
                    {{/if}}
                {{else if value.app_msg_ext_info.subtype == 16}} 
                <div id="WXVIDEO{{value.comm_msg_info.id}}" class="weui_media_box video js_video" hrefs="{{value.app_msg_ext_info.content_url}}">
                    <div class="weui_media_hd js_media" data-type="VIDEO">
                        <span class="video_cover" style="background-image:url({{value.app_msg_ext_info.cover}});height:{{height}}px;"></span>
                        <div class="video_switch">
                            {{if value.app_msg_ext_info.duration}}
                            <span class="video_time_info">{{handleVideoTime value.app_msg_ext_info.duration}}</span>
                            {{/if}}
                        </div>
                    </div>
                    <div class="weui_media_bd js_media" data-type="VIDEO">
                        <p class="weui_media_title">{{value.app_msg_ext_info.title}}</p>
                    </div>
                    <div class="weui_media_ft">
                        <p class="weui_media_extra_info">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日'}}</p>
                    </div>
                </div>
                {{/if}}

                {{each value.app_msg_ext_info.multi_app_msg_item_list as subvalue subkey}}
                {{if subvalue.del_flag == 4}}
                <div class="weui_media_box appmsg js_appmsg media_del" hrefs="{{subvalue.content_url}}">
                    <span class="weui_media_hd js_media" data-type="APPMSG"></span>
                    <div class="weui_media_bd js_media" data-type="APPMSG">
                        <h4 class="weui_media_title" hrefs="{{subvalue.content_url}}">内容违规已被删除</h4>
                        <p class="weui_media_desc"></p>
                        <p class="weui_media_extra_info">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日'}}</p>
                    </div>
                </div>
                {{else}}
                <div class="weui_media_box appmsg js_appmsg" hrefs="{{subvalue.content_url}}">
                    {{if subvalue.cover}}
                    <span class="weui_media_hd js_media" style="background-image:url({{subvalue.cover}})" data-s="300" data-type="APPMSG">
                      
                      
                    </span>
                    {{/if}}
                    <div class="weui_media_bd js_media" data-type="APPMSG">
                        <h4 class="weui_media_title" hrefs="{{subvalue.content_url}}">
                        {{if subvalue.copyright_stat==11}}<span id="copyright_logo" class="icon_original_tag">原创</span>
                        {{/if}}
                        {{subvalue.title}}
                        </h4>
                        <p class="weui_media_desc">{{subvalue.digest}}</p>
                        <p class="weui_media_extra_info">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日'}}{{if subvalue.copyright_stat==11}}<span id="copyright_logo" class="icon_original_tag">原创</span>{{/if}}</p>
                    </div>
                </div>
                {{/if}}
                {{/each}}
            </div>
        </div>
        {{/if}}
    {{/if}}
{{/each}}
</script>
</div>


        
        <script nonce="">
    var __DEBUGINFO = {
        debug_js : "//res.wx.qq.com/mmbizwap/en_US/htmledition/js/biz_wap/debug/console34c264.js",
        safe_js : "//res.wx.qq.com/mmbizwap/en_US/htmledition/js/biz_wap/safe/moonsafe34c264.js",
        res_list: []
    };
</script>

<script nonce="">
(function() {
	function _addVConsole(uri) {
		var url = '//res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/vconsole/' + uri;
		document.write('<script nonce="" type="text/javascript" src="' + url + '"><\/script>');
	}
	if (
		(document.cookie && document.cookie.indexOf('vconsole_open=1') > -1)
		|| location.href.indexOf('vconsole=1') > -1
	) {
		_addVConsole('2.5.1/vconsole.min.js');
		_addVConsole('plugin/vconsole-elements/1.0.2/vconsole-elements.min.js');
		_addVConsole('plugin/vconsole-sources/1.0.1/vconsole-sources.min.js');
		_addVConsole('plugin/vconsole-resources/1.0.0/vconsole-resources.min.js');
		_addVConsole('plugin/vconsole-mpopt/1.0.0/vconsole-mpopt.js');
	}
})();
</script>
        
        <script>window.__moon_host = 'res.wx.qq.com';window.moon_map = {"biz_common/utils/respTypes.js":"//res.wx.qq.com/mmbizwap/en_US/htmledition/js/biz_common/utils/respTypes3518c7.js","biz_common/utils/url/parse.js":"//res.wx.qq.com/mmbizwap/en_US/htmledition/js/biz_common/utils/url/parse36ebcf.js","biz_common/template-2.0.1-cmd.js":"//res.wx.qq.com/mmbizwap/en_US/htmledition/js/biz_common/template-2.0.1-cmd3518c7.js","biz_common/utils/wxgspeedsdk.js":"//res.wx.qq.com/mmbizwap/en_US/htmledition/js/biz_common/utils/wxgspeedsdk3518c7.js","biz_common/utils/emoji_data.js":"//res.wx.qq.com/mmbizwap/en_US/htmledition/js/biz_common/utils/emoji_data3518c7.js","biz_wap/utils/ajax.js":"//res.wx.qq.com/mmbizwap/en_US/htmledition/js/biz_wap/utils/ajax37cd32.js","history/profile_history_v2.html.js":"//res.wx.qq.com/mmbizwap/en_US/htmledition/js/history/profile_history_v2.html3803a4.js","biz_common/utils/string/html.js":"//res.wx.qq.com/mmbizwap/en_US/htmledition/js/biz_common/utils/string/html3518c7.js","history/template_helper.js":"//res.wx.qq.com/mmbizwap/en_US/htmledition/js/history/template_helper24f29e.js","common/zepto.js":"//res.wx.qq.com/mmbizwap/en_US/htmledition/js/common/zepto1b4b91.js","appmsg/cdn_img_lib.js":"//res.wx.qq.com/mmbizwap/en_US/htmledition/js/appmsg/cdn_img_lib373857.js","biz_wap/jsapi/core.js":"//res.wx.qq.com/mmbizwap/en_US/htmledition/js/biz_wap/jsapi/core34c264.js","history/performance.js":"//res.wx.qq.com/mmbizwap/en_US/htmledition/js/history/performance3717bc.js","biz_wap/utils/mmversion.js":"//res.wx.qq.com/mmbizwap/en_US/htmledition/js/biz_wap/utils/mmversion34c264.js","biz_common/dom/event.js":"//res.wx.qq.com/mmbizwap/en_US/htmledition/js/biz_common/dom/event36f1bc.js","history/profile_history_v2.js":"//res.wx.qq.com/mmbizwap/en_US/htmledition/js/history/profile_history_v237e20f.js","appmsg/profile.js":"//res.wx.qq.com/mmbizwap/en_US/htmledition/js/appmsg/profile384bec.js"};</script><script type="text/javascript">window.__wxgspeeds={}; window.__wxgspeeds.moonloadtime=+new Date()</script><script onerror="wx_loaderror(this)" type="text/javascript" src="//res.wx.qq.com/mmbizwap/en_US/htmledition/js/biz_wap/moon368f87.js"></script>
<script type="text/javascript">

    document.domain = "qq.com";
    var username = "" || "gh_a8bae9a81e16";
    var is_subscribed = "1" * 1;
    var action = "home" || "";
    var bizacct_type = "" || "";
    var can_msg_continue = '1' * 1;
    var headimg = "http://wx.qlogo.cn/mmhead/Q3auHgzwzM6NBSicQ1Lx4G9mN5yPZBicCFcaWJhEOtZpTHR1lLtVH8yg/0" || "";
    var nickname = "短史记" || "";
    var is_banned = "0" * 1;
    var __biz = "MzIzNTAyODMwOA==";
    var next_offset = "10" * 1;
    var use_demo = "0" * 1;
    
    
        var msgList = '{&quot;list&quot;:[{&quot;comm_msg_info&quot;:{&quot;id&quot;:1000000436,&quot;type&quot;:49,&quot;datetime&quot;:1504663452,&quot;fakeid&quot;:&quot;3235028308&quot;,&quot;status&quot;:2,&quot;content&quot;:&quot;&quot;},&quot;app_msg_ext_info&quot;:{&quot;title&quot;:&quot;大陆第一份“流行音乐排行榜”风波&nbsp;|&nbsp;短史记&quot;,&quot;digest&quot;:&quot;回望80年代。&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:502724184,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzIzNTAyODMwOA==&amp;amp;mid=2650207833&amp;amp;idx=1&amp;amp;sn=688a1dad339789efa88c328dab56c91a&amp;amp;chksm=f0ef3c62c798b574089319309b6b0c78fc8142edabe15d727f7f8b523cd0ca0c2b877fb47bb5&amp;amp;scene=27#wechat_redirect&quot;,&quot;source_url&quot;:&quot;&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz_jpg\\/ibYIE3gDV7K5J7Ll5H2IYNFgn94W20QNJZVrKEleac6NHUicrPibD5Z0pvUA2iaaBUWdUxrAvynppKrZ51ZQicoJeHw\\/0?wx_fmt=jpeg&quot;,&quot;subtype&quot;:9,&quot;is_multi&quot;:0,&quot;multi_app_msg_item_list&quot;:[],&quot;author&quot;:&quot;谌旭彬&quot;,&quot;copyright_stat&quot;:11,&quot;del_flag&quot;:1,&quot;item_show_type&quot;:0}},{&quot;comm_msg_info&quot;:{&quot;id&quot;:1000000435,&quot;type&quot;:49,&quot;datetime&quot;:1504567200,&quot;fakeid&quot;:&quot;3235028308&quot;,&quot;status&quot;:2,&quot;content&quot;:&quot;&quot;},&quot;app_msg_ext_info&quot;:{&quot;title&quot;:&quot;新版历史教科书：内中插图大有问题&nbsp;|&nbsp;短史记&quot;,&quot;digest&quot;:&quot;文字要求真，插图也同样要求真。&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:502724178,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzIzNTAyODMwOA==&amp;amp;mid=2650207828&amp;amp;idx=1&amp;amp;sn=848b6e30f39cc95be46b2423672b4fd3&amp;amp;chksm=f0ef3c6fc798b579d4c541ac9d6906483eff3e67241effb92bad7cda4b8cb0086344d2e9eff5&amp;amp;scene=27#wechat_redirect&quot;,&quot;source_url&quot;:&quot;&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz_jpg\\/ibYIE3gDV7K7ibvVQcSqkXPNvCd8GiarvQExBXN7LDozmKGLQXuAp9FIhFgav5fDEGCVEn1XS5APpIAo9XfdUhMfA\\/0?wx_fmt=jpeg&quot;,&quot;subtype&quot;:9,&quot;is_multi&quot;:0,&quot;multi_app_msg_item_list&quot;:[],&quot;author&quot;:&quot;谌旭彬&quot;,&quot;copyright_stat&quot;:11,&quot;del_flag&quot;:1,&quot;item_show_type&quot;:0}},{&quot;comm_msg_info&quot;:{&quot;id&quot;:1000000434,&quot;type&quot;:49,&quot;datetime&quot;:1504486611,&quot;fakeid&quot;:&quot;3235028308&quot;,&quot;status&quot;:2,&quot;content&quot;:&quot;&quot;},&quot;app_msg_ext_info&quot;:{&quot;title&quot;:&quot;百年前的原则：警察不可用大炮打麻雀&nbsp;|&nbsp;短史记&quot;,&quot;digest&quot;:&quot;违停。&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:502724175,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzIzNTAyODMwOA==&amp;amp;mid=2650207825&amp;amp;idx=1&amp;amp;sn=63da3ca8e3faba8ad9296982b8ef5d16&amp;amp;chksm=f0ef3c6ac798b57c9f4bb5f3e0734a5e91b62c38bb05ffe2d2e44a16e168dc0d14917336d58c&amp;amp;scene=27#wechat_redirect&quot;,&quot;source_url&quot;:&quot;&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz_jpg\\/ibYIE3gDV7K7ibvVQcSqkXPNvCd8GiarvQEpW37TibQfHA4EISwtweUQ0oxHs7pCeldwV2NDCJyVb1J5Q8uVarzTKA\\/0?wx_fmt=jpeg&quot;,&quot;subtype&quot;:9,&quot;is_multi&quot;:0,&quot;multi_app_msg_item_list&quot;:[],&quot;author&quot;:&quot;杨津涛&quot;,&quot;copyright_stat&quot;:11,&quot;del_flag&quot;:1,&quot;item_show_type&quot;:0}},{&quot;comm_msg_info&quot;:{&quot;id&quot;:1000000433,&quot;type&quot;:49,&quot;datetime&quot;:1504400240,&quot;fakeid&quot;:&quot;3235028308&quot;,&quot;status&quot;:2,&quot;content&quot;:&quot;&quot;},&quot;app_msg_ext_info&quot;:{&quot;title&quot;:&quot;抗战胜利的核心遗产：拥抱国际主流社会&nbsp;|&nbsp;短史记&quot;,&quot;digest&quot;:&quot;今天是抗战胜利纪念日。&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:502724170,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzIzNTAyODMwOA==&amp;amp;mid=2650207820&amp;amp;idx=1&amp;amp;sn=c5c92ff927ed7d961116cfd2c10b068d&amp;amp;chksm=f0ef3c77c798b5617bddb2dacd19efcc6725273e4064588c0efd05c8b22db9dd8d2eb01566f1&amp;amp;scene=27#wechat_redirect&quot;,&quot;source_url&quot;:&quot;&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz_jpg\\/ibYIE3gDV7K7Kq2tmLh2wiblfa88ldicZzZ0HoP8skH0icRB8nt6maXqScJBhGcqoX0edFp61XqaqtEVDuk4braPicA\\/0?wx_fmt=jpeg&quot;,&quot;subtype&quot;:9,&quot;is_multi&quot;:0,&quot;multi_app_msg_item_list&quot;:[],&quot;author&quot;:&quot;谌旭彬&quot;,&quot;copyright_stat&quot;:12,&quot;del_flag&quot;:1,&quot;item_show_type&quot;:0}},{&quot;comm_msg_info&quot;:{&quot;id&quot;:1000000432,&quot;type&quot;:49,&quot;datetime&quot;:1504317876,&quot;fakeid&quot;:&quot;3235028308&quot;,&quot;status&quot;:2,&quot;content&quot;:&quot;&quot;},&quot;app_msg_ext_info&quot;:{&quot;title&quot;:&quot;《国歌法》即将施行，回顾国歌六十年命运浮沉&nbsp;|&nbsp;短史记&quot;,&quot;digest&quot;:&quot;国歌歌词多次变更。&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:502724164,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzIzNTAyODMwOA==&amp;amp;mid=2650207813&amp;amp;idx=1&amp;amp;sn=5652beae29100ecea8b00af6b2f3be31&amp;amp;chksm=f0ef3c7ec798b568a9d5425a6b79bf21ac33ff04ff4081850844502566937bc4753c08cf92ec&amp;amp;scene=27#wechat_redirect&quot;,&quot;source_url&quot;:&quot;&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz_jpg\\/ibYIE3gDV7K57rZSXe6KhGgAkpeaRzQ9V44iabzNIDpBh4DqYmYPzyobaFZ4BKTAObpCdIcNDXxAgZXEUlT580sA\\/0?wx_fmt=jpeg&quot;,&quot;subtype&quot;:9,&quot;is_multi&quot;:0,&quot;multi_app_msg_item_list&quot;:[],&quot;author&quot;:&quot;周末休息的&quot;,&quot;copyright_stat&quot;:11,&quot;del_flag&quot;:1,&quot;item_show_type&quot;:0}},{&quot;comm_msg_info&quot;:{&quot;id&quot;:1000000431,&quot;type&quot;:49,&quot;datetime&quot;:1504238801,&quot;fakeid&quot;:&quot;3235028308&quot;,&quot;status&quot;:2,&quot;content&quot;:&quot;&quot;},&quot;app_msg_ext_info&quot;:{&quot;title&quot;:&quot;语文教材，究竟删减了多少鲁迅作品？&quot;,&quot;digest&quot;:&quot;鲁迅文章在教科书中被删减，原因何在？&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:502724160,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzIzNTAyODMwOA==&amp;amp;mid=2650207809&amp;amp;idx=1&amp;amp;sn=fc09816077922674c498fc1ddb9da2da&amp;amp;chksm=f0ef3c7ac798b56c6344b1c09cfc5f79ced78b9d17eefe104ab1b15d79ce8372df1f9a3b890d&amp;amp;scene=27#wechat_redirect&quot;,&quot;source_url&quot;:&quot;&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz_jpg\\/ibYIE3gDV7K4UrBl3dbkIZJGh81GbLwUhrJ47lXPH6VzFANDA7Ne2eXsB8aQgszz9QnRg0ia9fQLrh8gMicyib3Irw\\/0?wx_fmt=jpeg&quot;,&quot;subtype&quot;:9,&quot;is_multi&quot;:0,&quot;multi_app_msg_item_list&quot;:[],&quot;author&quot;:&quot;杨津涛&quot;,&quot;copyright_stat&quot;:11,&quot;del_flag&quot;:1,&quot;item_show_type&quot;:0}},{&quot;comm_msg_info&quot;:{&quot;id&quot;:1000000430,&quot;type&quot;:49,&quot;datetime&quot;:1504143679,&quot;fakeid&quot;:&quot;3235028308&quot;,&quot;status&quot;:2,&quot;content&quot;:&quot;&quot;},&quot;app_msg_ext_info&quot;:{&quot;title&quot;:&quot;“彭德怀打金日成耳光”之说很可疑&nbsp;|&nbsp;短史记&quot;,&quot;digest&quot;:&quot;目前流传的版本，经不起史料检验。&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:502724153,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzIzNTAyODMwOA==&amp;amp;mid=2650207802&amp;amp;idx=1&amp;amp;sn=ca1732420201717b86ae8f2524d1ebb9&amp;amp;chksm=f0ef3c01c798b5178e31771120fc0eb54059acfa0a612e9e4a519b80906d91938263bc7ca810&amp;amp;scene=27#wechat_redirect&quot;,&quot;source_url&quot;:&quot;&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz_jpg\\/ibYIE3gDV7K6NYHkE6wOzmLH2WrcGa9V5FyjcDELhBgvwKjyiccBsQs1gecKxMCNAkhk7tlysQyWPkBU8Yez29MQ\\/0?wx_fmt=jpeg&quot;,&quot;subtype&quot;:9,&quot;is_multi&quot;:0,&quot;multi_app_msg_item_list&quot;:[],&quot;author&quot;:&quot;谌旭彬&quot;,&quot;copyright_stat&quot;:11,&quot;del_flag&quot;:1,&quot;item_show_type&quot;:0}},{&quot;comm_msg_info&quot;:{&quot;id&quot;:1000000429,&quot;type&quot;:49,&quot;datetime&quot;:1504057331,&quot;fakeid&quot;:&quot;3235028308&quot;,&quot;status&quot;:2,&quot;content&quot;:&quot;&quot;},&quot;app_msg_ext_info&quot;:{&quot;title&quot;:&quot;人工智能进入小学课程，曾长期被打成“伪科学”&nbsp;|&nbsp;短史记&quot;,&quot;digest&quot;:&quot;这一代小学生，左手“人工智能”，右手“黄帝内经，你们怕不怕？&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:502724148,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzIzNTAyODMwOA==&amp;amp;mid=2650207798&amp;amp;idx=1&amp;amp;sn=67bb56003db45562461b355477a553e2&amp;amp;chksm=f0ef3c0dc798b51bf9c67cd9a3245b6297f0a3a08a930f831a61f62ada6145b338cdd61c4738&amp;amp;scene=27#wechat_redirect&quot;,&quot;source_url&quot;:&quot;&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz_jpg\\/ibYIE3gDV7K7gpovO7kCp578I7BIjWORK287n655qkI32fM9DxyZtEYPK1IibC2DibxicNVnQ2PxVEW5E23WoH4fKg\\/0?wx_fmt=jpeg&quot;,&quot;subtype&quot;:9,&quot;is_multi&quot;:0,&quot;multi_app_msg_item_list&quot;:[],&quot;author&quot;:&quot;谌旭彬&quot;,&quot;copyright_stat&quot;:11,&quot;del_flag&quot;:1,&quot;item_show_type&quot;:0}},{&quot;comm_msg_info&quot;:{&quot;id&quot;:1000000428,&quot;type&quot;:49,&quot;datetime&quot;:1503967883,&quot;fakeid&quot;:&quot;3235028308&quot;,&quot;status&quot;:2,&quot;content&quot;:&quot;&quot;},&quot;app_msg_ext_info&quot;:{&quot;title&quot;:&quot;“以法治孝”的秦王朝，反而不孝成风&nbsp;|&nbsp;短史记&quot;,&quot;digest&quot;:&quot;秦人“不孝”的根本原因，在于秦制，在于其弱民、贫民政策。&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:502724142,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzIzNTAyODMwOA==&amp;amp;mid=2650207792&amp;amp;idx=1&amp;amp;sn=268d83b1ad0cb2a05593759315e8ea87&amp;amp;chksm=f0ef3c0bc798b51d54f0703a82e8ac7f83555e1231cdd4a6710e6dfb55f3efd93b4e61ba9f7b&amp;amp;scene=27#wechat_redirect&quot;,&quot;source_url&quot;:&quot;&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz_jpg\\/ibYIE3gDV7K5lpbg7O3RRibj70ibguBRW3ic4aJ94W8UMQtD8Wky5sduIrh2UOibLeYEiajBFQVAk9pvnVzvFBWsRu4Q\\/0?wx_fmt=jpeg&quot;,&quot;subtype&quot;:9,&quot;is_multi&quot;:0,&quot;multi_app_msg_item_list&quot;:[],&quot;author&quot;:&quot;谌旭彬&quot;,&quot;copyright_stat&quot;:11,&quot;del_flag&quot;:1,&quot;item_show_type&quot;:0}},{&quot;comm_msg_info&quot;:{&quot;id&quot;:1000000427,&quot;type&quot;:49,&quot;datetime&quot;:1503883385,&quot;fakeid&quot;:&quot;3235028308&quot;,&quot;status&quot;:2,&quot;content&quot;:&quot;&quot;},&quot;app_msg_ext_info&quot;:{&quot;title&quot;:&quot;蒋介石为何秘密给胡适送钱？&nbsp;|&nbsp;短史记&quot;,&quot;digest&quot;:&quot;此种内因，不可对外解释。&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:502724137,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzIzNTAyODMwOA==&amp;amp;mid=2650207787&amp;amp;idx=1&amp;amp;sn=820bad445031f58ccc11eab7d745c6bb&amp;amp;chksm=f0ef3c10c798b5063b54d2a8f16683b535381128c3870efd18952ee8e157b46301cba85e5106&amp;amp;scene=27#wechat_redirect&quot;,&quot;source_url&quot;:&quot;&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz_jpg\\/ibYIE3gDV7K7h29TyW7MqdJQ8DEoF8lazarwKpySV9Q6qGKJLVxjciaJfZMOr9HYyiaF3gyDTdj5V18UQDZAqK6Dw\\/0?wx_fmt=jpeg&quot;,&quot;subtype&quot;:9,&quot;is_multi&quot;:0,&quot;multi_app_msg_item_list&quot;:[],&quot;author&quot;:&quot;谌旭彬&quot;,&quot;copyright_stat&quot;:11,&quot;del_flag&quot;:1,&quot;item_show_type&quot;:0}}]}';
        if(!!window.__initCatch){
        window.__initCatch({
            idkey : 29711,
            startKey : 60,
            badjsId: 47,
            
            reportOpt : {
                username : username,
            }
        });
    }
                        
    seajs.use("appmsg/profile.js");
</script>

    
    <script nonce="" type="text/javascript">document.addEventListener("touchstart", function() {},false);</script>

<!--tailTrap<body></body><head></head><html></html>-->
<span style="border-radius: 3px; text-indent: 20px; width: auto; padding: 0px 4px 0px 0px; text-align: center; font-style: normal; font-variant: normal; font-weight: bold; font-stretch: normal; font-size: 11px; line-height: 20px; font-family: &quot;Helvetica Neue&quot;, Helvetica, sans-serif; color: rgb(255, 255, 255); background: url(&quot;data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMzBweCIgd2lkdGg9IjMwcHgiIHZpZXdCb3g9Ii0xIC0xIDMxIDMxIj48Zz48cGF0aCBkPSJNMjkuNDQ5LDE0LjY2MiBDMjkuNDQ5LDIyLjcyMiAyMi44NjgsMjkuMjU2IDE0Ljc1LDI5LjI1NiBDNi42MzIsMjkuMjU2IDAuMDUxLDIyLjcyMiAwLjA1MSwxNC42NjIgQzAuMDUxLDYuNjAxIDYuNjMyLDAuMDY3IDE0Ljc1LDAuMDY3IEMyMi44NjgsMC4wNjcgMjkuNDQ5LDYuNjAxIDI5LjQ0OSwxNC42NjIiIGZpbGw9IiNmZmYiIHN0cm9rZT0iI2ZmZiIgc3Ryb2tlLXdpZHRoPSIxIj48L3BhdGg+PHBhdGggZD0iTTE0LjczMywxLjY4NiBDNy41MTYsMS42ODYgMS42NjUsNy40OTUgMS42NjUsMTQuNjYyIEMxLjY2NSwyMC4xNTkgNS4xMDksMjQuODU0IDkuOTcsMjYuNzQ0IEM5Ljg1NiwyNS43MTggOS43NTMsMjQuMTQzIDEwLjAxNiwyMy4wMjIgQzEwLjI1MywyMi4wMSAxMS41NDgsMTYuNTcyIDExLjU0OCwxNi41NzIgQzExLjU0OCwxNi41NzIgMTEuMTU3LDE1Ljc5NSAxMS4xNTcsMTQuNjQ2IEMxMS4xNTcsMTIuODQyIDEyLjIxMSwxMS40OTUgMTMuNTIyLDExLjQ5NSBDMTQuNjM3LDExLjQ5NSAxNS4xNzUsMTIuMzI2IDE1LjE3NSwxMy4zMjMgQzE1LjE3NSwxNC40MzYgMTQuNDYyLDE2LjEgMTQuMDkzLDE3LjY0MyBDMTMuNzg1LDE4LjkzNSAxNC43NDUsMTkuOTg4IDE2LjAyOCwxOS45ODggQzE4LjM1MSwxOS45ODggMjAuMTM2LDE3LjU1NiAyMC4xMzYsMTQuMDQ2IEMyMC4xMzYsMTAuOTM5IDE3Ljg4OCw4Ljc2NyAxNC42NzgsOC43NjcgQzEwLjk1OSw4Ljc2NyA4Ljc3NywxMS41MzYgOC43NzcsMTQuMzk4IEM4Ljc3NywxNS41MTMgOS4yMSwxNi43MDkgOS43NDksMTcuMzU5IEM5Ljg1NiwxNy40ODggOS44NzIsMTcuNiA5Ljg0LDE3LjczMSBDOS43NDEsMTguMTQxIDkuNTIsMTkuMDIzIDkuNDc3LDE5LjIwMyBDOS40MiwxOS40NCA5LjI4OCwxOS40OTEgOS4wNCwxOS4zNzYgQzcuNDA4LDE4LjYyMiA2LjM4NywxNi4yNTIgNi4zODcsMTQuMzQ5IEM2LjM4NywxMC4yNTYgOS4zODMsNi40OTcgMTUuMDIyLDYuNDk3IEMxOS41NTUsNi40OTcgMjMuMDc4LDkuNzA1IDIzLjA3OCwxMy45OTEgQzIzLjA3OCwxOC40NjMgMjAuMjM5LDIyLjA2MiAxNi4yOTcsMjIuMDYyIEMxNC45NzMsMjIuMDYyIDEzLjcyOCwyMS4zNzkgMTMuMzAyLDIwLjU3MiBDMTMuMzAyLDIwLjU3MiAxMi42NDcsMjMuMDUgMTIuNDg4LDIzLjY1NyBDMTIuMTkzLDI0Ljc4NCAxMS4zOTYsMjYuMTk2IDEwLjg2MywyNy4wNTggQzEyLjA4NiwyNy40MzQgMTMuMzg2LDI3LjYzNyAxNC43MzMsMjcuNjM3IEMyMS45NSwyNy42MzcgMjcuODAxLDIxLjgyOCAyNy44MDEsMTQuNjYyIEMyNy44MDEsNy40OTUgMjEuOTUsMS42ODYgMTQuNzMzLDEuNjg2IiBmaWxsPSIjYmQwODFjIj48L3BhdGg+PC9nPjwvc3ZnPg==&quot;) 3px 50% / 14px 14px no-repeat rgb(189, 8, 28); position: absolute; opacity: 1; z-index: 8675309; display: none; cursor: pointer; border: none; -webkit-font-smoothing: antialiased;">Save</span><span style="width: 24px; height: 24px; background: url(&quot;data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pjxzdmcgd2lkdGg9IjI0cHgiIGhlaWdodD0iMjRweCIgdmlld0JveD0iMCAwIDI0IDI0IiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxkZWZzPjxtYXNrIGlkPSJtIj48cmVjdCBmaWxsPSIjZmZmIiB4PSIwIiB5PSIwIiB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHJ4PSI2IiByeT0iNiIvPjxyZWN0IGZpbGw9IiMwMDAiIHg9IjUiIHk9IjUiIHdpZHRoPSIxNCIgaGVpZ2h0PSIxNCIgcng9IjEiIHJ5PSIxIi8+PHJlY3QgZmlsbD0iIzAwMCIgeD0iMTAiIHk9IjAiIHdpZHRoPSI0IiBoZWlnaHQ9IjI0Ii8+PHJlY3QgZmlsbD0iIzAwMCIgeD0iMCIgeT0iMTAiIHdpZHRoPSIyNCIgaGVpZ2h0PSI0Ii8+PC9tYXNrPjwvZGVmcz48cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIGZpbGw9IiNmZmYiIG1hc2s9InVybCgjbSkiLz48L3N2Zz4=&quot;) 50% 50% / 14px 14px no-repeat rgba(0, 0, 0, 0.4); position: absolute; opacity: 1; z-index: 8675309; display: none; cursor: pointer; border: none; border-radius: 12px;"></span></body></html>
```



