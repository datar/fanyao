# coding: utf-8

import requests
import urllib
import html
import lxml.html
import lxml.html.clean
import json
import os
import os.path
import sys
import shutil
import re
import multiprocessing.dummy

re_date = re.compile('(\d{4})\D+(\d{1,2})\D+(\d{1,2})')
resource = []
re_url_special_ch = re.compile('[:\/\&\?#]+')

FILENAME_ORIGIN = 'origin_page.html'
FILENAME_CLEAN = 'clean_page.html'
FILENAME_CLEAN_FORMAT = 'format_clean_page.html'

def get_msg_links_from_history_page(page_text, base_dir):
    links = []
    document = lxml.html.fromstring(page_text)
    nodes = document.xpath("//div[@class='weui_msg_card js_card']")
    for node in nodes:
        date_str = transfer_date_str(node.xpath("*[@class='weui_msg_card_hd']")[0].text)
        msgid = node.get('msgid')
        items = node.xpath("*//*[@class='weui_media_title']")
        for i in range(len(items)):
            link = items[i].get('hrefs')
            for c in items[i].xpath("*[@id='copyright_logo']"):
                origin_lab = True
                c.drop_tree()
            title = re.sub('\s', '', items[i].text_content())
            item_id = msgid + '_' + str(i)
            dir_name = '[wechat][%s][%s]'%(date_str, item_id)
            dir_name = os.path.join(base_dir, dir_name)
            if not os.path.exists(dir_name):
                os.makedirs(dir_name)
            origin_file_path = os.path.join(dir_name, FILENAME_ORIGIN)
            clean_file_path = os.path.join(dir_name, FILENAME_CLEAN)
            format_clean_file_path = os.path.join(dir_name, FILENAME_CLEAN_FORMAT)
            links.append((date_str, item_id, title, link, origin_file_path, clean_file_path, format_clean_file_path))
    return links

def transfer_date_str(date_str):
    slices = list(re_date.search(date_str).groups())
    if len(slices) == 3:
        if len(slices[1]) < 2:
            slices[1] = '0'+slices[1]
        if len(slices[2]) < 2:
            slices[2] = '0'+slices[2]
        return ''.join(slices)
    else:
        return '19700101'

def clean_wechat_page(text):
    document = lxml.html.fromstring(text)
    for node in document.xpath("//img"):
        if node.get('src') is None and node.get('data-src') is not None:
            node.set('src', node.get('data-src'))
    for node in document.xpath("//script"):
        node.getparent().remove(node)
    for node in document.xpath("//i"):
        node.getparent().remove(node)
    for node in document.xpath("//title"):
        node.getparent().remove(node)
    for node in document.xpath("//style"):
        node.getparent().remove(node)
    minipro_div = document.get_element_by_id('js_minipro_dialog', None)
    if minipro_div is not None:
        minipro_div.getparent().remove(minipro_div)
    return lxml.html.tostring(document)

def generate_filename_by_url(url):
    result = url.replace('=', '.')
    return re_url_special_ch.sub('-', result)

def download_image_wechat_page(text):
    document = lxml.html.fromstring(text)
    images = []
    for node in document.xpath("//img"):
        url = node.get('src', default=None)
        if url is not None and len(url) > 20:
            if not url.startswith('http'):
                url = 'http:' + url
            filename = generate_filename_by_url(url)
            node.set('src', filename)
            images.append((url, filename))
    return lxml.html.tostring(document), images

def save_origin_msg(item):
    link = item[3]
    title = item[2]
    date_str = item[0]
    item_id = item[1]
    page = requests.get(link).text
    dst_file = item[4]
    with open(dst_file, 'w') as f:
        f.write(page)

def save_image_from_url_to_file(url, filepath):
    try:
        data = requests.get(url)
        with open(filepath, 'wb') as f:
            f.write(data.content)        
    except Exception as e:
        print(e)
        print("something wrong in: %s\n%s"%(url, filepath))
        
def clean_format_msg(item):
    text = open(item[4]).read()
    cleantext = clean_wechat_page(text)
    with open(item[5], 'wb') as f:
        f.write(cleantext)
    format_text, images = download_image_wechat_page(cleantext)
    with open(item[6], 'wb') as f:
        f.write(format_text)
    for url, filename in images:
        filepath = os.path.join(os.path.dirname(item[6]), filename)
        save_image_from_url_to_file(url, filepath)
        
        
def parse_msg(item):
    save_origin_msg(item)
    clean_format_msg(item)
            
            
def save_msgs(items):
    for item in items:
        parse_msg(item)
        
def save_msgs_multi(items):
    pool = multiprocessing.dummy.Pool(50) 
    r =pool.map(parse_msg, items) 
    pool.close()
    pool.join()



def save_official_account_from_file(filepath, dst_path):
    page_text = open(filepath).read()
    items = get_msg_links_from_history_page(page_text, dst_path)
    save_msgs_multi(items)



def main():
    if len(sys.argv) < 3:
        print("Usage: wechat_account.py <dir_path> <file_name>")
        return 0
    base_dir = sys.argv[1]
    filepath = os.path.join(base_dir, sys.argv[2])
    save_official_account_from_file(filepath, base_dir)


if __name__ == '__main__':
    main()